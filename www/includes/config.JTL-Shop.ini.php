<?php
define('PFAD_ROOT', 'C:\\xampp\\htdocs\\jtl-shop-bootstrapper\\www/');
define('URL_SHOP', 'http://jtl-shop-bootstrapper.local');
define('DB_HOST','localhost');
define('DB_NAME','myDb');
define('DB_USER','root');
define('DB_PASS','test');

define('BLOWFISH_KEY', '97cf916fbe50f04c84b093d1e1e26e');

//enables printing of warnings/infos/errors for the shop frontend
define('SHOP_LOG_LEVEL', 0);
//enables printing of warnings/infos/errors for the dbeS sync
define('SYNC_LOG_LEVEL', 0);
//enables printing of warnings/infos/errors for the admin backend
define('ADMIN_LOG_LEVEL', 0);
//enables printing of warnings/infos/errors for the smarty templates
define('SMARTY_LOG_LEVEL', 0);
//excplicitly show/hide errors
ini_set('display_errors', 0);

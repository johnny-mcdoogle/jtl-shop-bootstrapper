<?php

declare(strict_types=1);

namespace Plugin\foo;

use JTL\Events\Dispatcher;
use JTL\Plugin\Bootstrapper;
use Jtllog;

class Bootstrap extends Bootstrapper
{
    public function installed()
    {
        parent::installed();

        Jtllog::writeLog('installed');
    }

    public function uninstalled(bool $deleteData = true)
    {
        parent::uninstalled();

        Jtllog::writeLog('uninstalled');
    }

    public function boot(Dispatcher $dispatcher)
    {
        parent::boot($dispatcher);

        Jtllog::writeLog('boot');
    }
}
